// Dependencies
const express = require("express");
const router = express.Router();
const auth = require("../auth");

// for importing courseController
const courseController = require("../controllers/courseController")

// Router for creating course
// auth.verify will verify if this has token
router.post("/addCourse", auth.verify, (req,res) => {

    const data = {
        course: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    courseController.addCourse(data).then(result => res.send(result))
});

// Activity
// Router for getting all courses that will be used by admin only
// auth.verify is used for token checking
// no need to input req.body since you will only get all the details in courses. you just need it if you want to get specific details.
router.get("/", (req,res) => {
    courseController.getCourse().then(result => res.send(result))
});


// Route for updating the course 
// auth.verify is used for token checking
router.put("/:courseId", auth.verify, (req,res) => {

    courseController.updateCourse(req.params, req.body).then(result => res.send(result));
});


// Route for archiving courses
// using params only
// if you want to update it by inputing on the body you can use the req.body
router.put("/archive/:courseId", auth.verify, (req,res) => {

    courseController.archiveCourse(req.params).then(result => res.send(result));
});


// Route for unarchiving courses
// using params only
// if you want to update it by inputing on the body you can use the req.body
router.patch("/unArchive/:courseId", auth.verify, (req,res) => {

    courseController.archiveCourse(req.params).then(result => res.send(result));
});

// routes for getting all active courses
// for users only on getting all courses
router.get("/getAllActive", auth.verify, (req,res) => {

    courseController.getAllActive().then(result => res.send(result));
});





// for exporting
module.exports = router;