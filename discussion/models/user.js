const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

    firstName: {
        // type is use to declare what kind of data type
        type: String,
        // required is use to require the user to fill this up
        required: [true, "First name is required"]

    },
    lastName: {
        type: String,
        required: [true, "Last name is required"]
    },
    email: {
        type: String,
        required: [true, "Email is required"]
    },
    password: {
        type: String,
        required: [true, "Password is required"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    mobileNo: {
        type: String,
        required: [true, "Mobile number is required"]
    },
    enrollments: [
        {
            courseId : {
                type : String,
                required: [true, "Course ID is required"]
            },
            enrolledOn : {
                type: Date,
                default: new Date()
            },
            status: {
                type: String,
                Default: "Enrolled"
            }
        }
    ]

});

// this will export the schema
// "User" will be the collection name in Database and will be automatically convert to plural form
// userSchema call the schema name that created earlier
module.exports = mongoose.model("User", userSchema)