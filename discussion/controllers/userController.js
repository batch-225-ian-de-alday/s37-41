const User = require("../models/user");
const bcrypt = require("bcrypt");
const Course = require("../models/course");

// import the auth.js
const auth = require("../auth");

/* // for findOne
module.exports.checkEmailExists = (reqBody) => {

    return User.findOne({email : reqBody.email}).then(result => {

        // the findOne method returns a record if a match is found

        if (result.length > 0) {

            return true
             
        } else {
        
            return false;
        }
    })
};
 */

// for find 
module.exports.checkEmailExists = (reqBody) => {

    return User.find({email : reqBody.email}).then(result => {

        // the find method returns a record if a match is found

        if (result.length === 1) {

            return {
                message: "User was found"
            }
             
        } else if (result.length > 1) {

            return {
                message: "Duplicate User"
            }
        }
        
        else {
        
            return {
                message: "User not found"
            }
        }
    })
};

// for regestering users
module.exports.registerUser = (reqBody) => {

    let newUser = new User ({
        firstName : reqBody.firstName,
        lastName : reqBody.lastName,
        email : reqBody.email,
        mobileNo : reqBody.mobileNo,
        password : bcrypt.hashSync(reqBody.password,10)
    })

    return newUser.save().then((user, error) => {
        if (error) {

            return false;

        } else {

            return user;
        };
    });
};


// User Authentication
/* 
    Steps:
    1. Check the database if the user is email exists
    2. Compare the password provided in the login form with the password stored with the database
    3. Generate/ return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) => {

    return User.findOne({email :reqBody.email}).then(result => {

        if (result == null) {
            
            return {
                message: "Email not found in our database"
            }

        } else {

            // The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password) 

            if (isPasswordCorrect) {

                return {access : auth.createAccessToken(result)}

            } else {

                // if password doesn't match
                return {
                    message: "Password was incorrect"
                }
            };
        };
    });
}



//===========Activity 2=================
// finding by ID and return the passsword to empty string

module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {
        result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});

}


// enrolment
module.exports.enroll = async (data) => {

    let isUserUpdated = await User.findById(data.userId).then(user => {

        user.enrollments.push({courseId : data.courseId});
    
        return user.save().then((user, error) => {

            if (error) {

                return false;

            } else {

                return true;
            };
        });
    });

    // Add the user ID in the enrollees array of the course
	// Using the "await" keyword will allow the enroll method to complete updating the course before returning a response back to the frontend
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

		// Adds the userId in the course's enrollees array
		course.enrollees.push({userId : data.userId});

        // Saves the updated course information in the database
		return course.save().then((course, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});

	});

    // Condition that will check if the user and course documents have been updated
	// User enrollment successful
	if(isUserUpdated && isCourseUpdated){
		return {
            message: "You're enrolled"
        }
	// User enrollment failure
	} else {
		return false;
	};
    
}

// =================================

// this will find the id and update the password this can be used on patching
/* module.exports.getProfile = (reqBody) => {
    
    return User.findByIdAndUpdate({_id: reqBody.id},{password : ""}).then((result) => {
        
        return result       
    })
}; */

// another solution with error handler
// note that if you use reqBody in finding you must type _id in your json in postman
/* module.exports.getProfile = (reqBody) => {

    return User.findByIdAndUpdate(reqBody,{password : ""}).then((result, err) => {
        
        if (err) {

            console.log(err);
            return false;

        } else {
            return result
        }
    })
};  */

// for getting all users details only firstName and lastName only will be shown
/* module.exports.getAllUsersDetails = () => {
    return User.find({}).then(result => {
        
        // This will only return the firstname and lastname of all users
        new_result = result.map(user => {
            return (({ firstName, lastName }) => ({ firstName, lastName }))(user);
        })
        return new_result;
    })
};
 */

// ===================================


 


