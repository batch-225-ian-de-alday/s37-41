// for importing the Course model
const Course = require("../models/course");


module.exports.addCourse = (data) => {
	if(data.isAdmin){
		// use new because it is creating a new data or courses
		let new_course = new Course ({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		})

		return new_course.save().then((new_course, error) => {
			if(error){
				return false
			}

			return {
				message: 'New course successfully created!'
			}
		})
	} 

	let message = Promise.resolve('User must me Admin to Access this.')

	return message.then((value) => {
		return value

	// you can use this to simplify code
	// Promise.resolve('User must me Admin to Access this.')
    // this is use as error handler
	})
		
}

// ACTIVITY

// Controller function that retrieve all course

module.exports.getCourse = (req) => {

    return Course.find({}).then((result) => {
        return result;
    });
};


// method for updating course
module.exports.updateCourse = (reqParams, reqBody) => {

	// specify the fields/properties of the document to be updated
	// no need to use new since it is updating course only

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	/* 
		Syntax:

		findByIdAndUpdate(document ID,  updatesTobeApplied)
	*/

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {

	// course not updated

		if (error) { 
			return false;
			
		} else {
			return {
				message: "Course updated successfully"
			};
		};
	});
};

// Archive course method
module.exports.archiveCourse = (reqParams) => {

	let updatedActiveField = {

		isActive: false
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updatedActiveField).then((course, error) => {

		if (error) {

			return false;

		} else {

			return {
				message: "Archiving Course Successfully"
			}
		};
	});
};

// unarchiving course method
module.exports.archiveCourse = (reqParams) => {

	let updatedActiveField = {

		isActive: true
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updatedActiveField).then((course, error) => {

		if (error) {

			return false;

		} else {

			return {
				message: "Unarchiving Course Successfully"
			}
		};
	});
};



// function for getting all active courses
module.exports.getAllActive = () => {

	// Mini Activity
	

	return Course.find({isActive : true}).then((result) => {
		return result})
}



